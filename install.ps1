## @todo(stdmatt): Add the license header.... 3/17/2021, 11:00:20 AM

##
## Constants
##
##------------------------------------------------------------------------------
## Script
$SCRIPT_FULLPATH = $MyInvocation.MyCommand.Path;
$SCRIPT_DIR      = Split-Path "$SCRIPT_FULLPATH" -Parent;
$HOME_DIR        = "$env:USERPROFILE";
## Program
$PROGRAM_NAME           = "saneswitch";
$PROGRAM_BUILD_PATH     = "$SCRIPT_DIR/build.win32";
$PROGRAM_BUILD_EXE_PATH = "$SCRIPT_DIR/build.win32/Release/$PROGRAM_NAME.exe";
$PROGRAM_INSTALL_PATH   = "$HOME_DIR/.stdmatt_bin/$PROGRAM_NAME";

##
## Script
##
##------------------------------------------------------------------------------
echo "Building..."

## Create the Build directory...
if (-not (Test-Path -LiteralPath $PROGRAM_BUILD_PATH)) {
    echo "Creating directory at: ";
    echo "    $PROGRAM_BUILD_PATH";
    [void](New-Item -Path $PROGRAM_BUILD_PATH -ItemType Directory);
}

## Build.
cd $PROGRAM_BUILD_PATH
    cmake ..
    cmake --build . --target ALL_BUILD --config Release
cd ..

echo "";

echo "Installing ...";
if (-not (Test-Path -LiteralPath $PROGRAM_BUILD_EXE_PATH)) {
    echo "Failed to build binary at:";
    echo "    $PROGRAM_BUILD_EXE_PATH";
    echo "Aborting...";

    return;
}

## Create the install directory...
if (-not (Test-Path -LiteralPath $PROGRAM_INSTALL_PATH)) {
    echo "Creating directory at: ";
    echo "    $PROGRAM_INSTALL_PATH";
    [void](New-Item -Path $PROGRAM_INSTALL_PATH -ItemType Directory);
}

## Copy the file to the install dir...
cp -Force $PROGRAM_BUILD_EXE_PATH $PROGRAM_INSTALL_PATH

echo "$PROGRAM_NAME was installed at:";
echo "    $PROGRAM_INSTALL_PATH";
echo "You might need add it to the PATH";

echo "Done... ;D";
echo "";
