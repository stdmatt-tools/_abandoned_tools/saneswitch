// Header
#include "Tray.hpp"
// saneswitch
#include "Error.hpp"
#include "app_icon.h"

//
// Constants
//
ARK_STRICT_CONSTEXPR u32 TRAY_ICON_UID = 100;
ARK_STRICT_CONSTEXPR u32 MENU_ITEM_ID  = 1234;

//------------------------------------------------------------------------------
HICON
Tray::LoadIcon(ark::String const &filename)
{
    // Create the icon file if it doesn't exist.
    // @todo(stdmatt): Find a way to load the image from the buffer, so we don't need to create nothing - April 02, 2021
    if(!ark::PathUtils::IsFile(filename)) {
        ark::PathUtils::CreateDir(
            ark::PathUtils::Dirname(filename),
            ark::PathUtils::CreateDirOptions::Recursive
        );
        ark::FileUtils::WriteResult_t const result = ark::FileUtils::WriteBuffer(
            filename,
            APP_ICON_ICO,
            APP_ICON_ICO_LEN,
            ark::FileUtils::WriteMode::Overwrite
        );

        if(result.HasFailed()) {
            ark::Fatal("Failed to generate the icon at: ({})", filename);
        }
    }

    return ark::Win32::LoadIconFromFile(GetModuleHandle(nullptr), filename);
}

//------------------------------------------------------------------------------
void
Tray::DrawIcon(HICON const icon_handle, HWND const window_handle)
{
    NOTIFYICONDATA nid = {};

    nid.cbSize   = sizeof(NOTIFYICONDATA);
    nid.uFlags   = NIF_MESSAGE | NIF_ICON | NIF_TIP;
    nid.uVersion = NOTIFYICON_VERSION;
    nid.hWnd     = window_handle;
    nid.uID      = TRAY_ICON_UID;
    nid.hIcon    = icon_handle;

    nid.uCallbackMessage = Tray::WM_TRAY;
    // LoadString(app_instance, IDS_APP_TITLE, nid.szTip, 128);
    Shell_NotifyIcon(NIM_ADD, &nid);
}

//------------------------------------------------------------------------------
void
Tray::HideIcon()
{

}

void add_or_update_menu_item(
    HMENU       const menu_handle,
    ark::String const &title)
{
}

//------------------------------------------------------------------------------
void
Tray::HandleTrayEvents(TrayEvent_t const &event)
{
    if(event.l_param == WM_RBUTTONDOWN) {
        ark::String const title       = "Exit...";
        HMENU       const menu_handle = CreatePopupMenu();

        MENUITEMINFO menu_item_info = {};

        menu_item_info.cbSize     = sizeof(MENUITEMINFO);
        menu_item_info.fMask      = MIIM_FTYPE | MIIM_STRING | MIIM_DATA | MIIM_STATE | MIIM_SUBMENU | MIIM_ID;
        menu_item_info.fType      = MFT_STRING;
        menu_item_info.dwTypeData = Cast<LPSTR>(title.CStr());
        menu_item_info.cch        = Cast<u32>(title.Length() + 1);
        menu_item_info.dwItemData = Cast<ULONG_PTR>(MENU_ITEM_ID);
        menu_item_info.fState     = 0;
        menu_item_info.wID        = MENU_ITEM_ID;

        POINT point = {};
        GetCursorPos(&point);

        InsertMenuItem(menu_handle, MENU_ITEM_ID, true, &menu_item_info);
        SetForegroundWindow(event.window_handle);
        auto const result = TrackPopupMenu(
            menu_handle,
            TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_RIGHTBUTTON | TPM_RETURNCMD,
            point.x, point.y,
            0,
            event.window_handle,
            nullptr
        );

        PostMessage(event.window_handle, WM_NULL, 0, 0);
        DestroyMenu(menu_handle);

        if(result == MENU_ITEM_ID) {
            SendMessage(event.window_handle, WM_CLOSE, 0, 0);
        }
    }
}
