#pragma once
// Arkadia
#include "ark_core/ark_core.hpp"

namespace Switcher {

//
// Constants
//
//------------------------------------------------------------------------------
constexpr u32 INVALID_INDEX = ark::Max<u32>();

//
// Types
//
//------------------------------------------------------------------------------
struct SwitchData
{
    HWND             fg_window_handle = nullptr;
    ark::Array<HWND> matching_window_handles;
    ark::String      target_exe_name;

    u32  current_index  = 0;
    u32  original_index = 0;

    bool has_data = false;
}; // struct SwitchData

//
// Functions
//
//------------------------------------------------------------------------------
void CreateData (SwitchData *data);
void DestroyData(SwitchData *data);
void Switch     (SwitchData *data, bool const backwards);

} // namespace Switcher
