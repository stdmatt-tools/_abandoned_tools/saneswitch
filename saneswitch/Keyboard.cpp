// Header
#include "Keyboard.hpp"
//
#include "Error.hpp"
#include "ark_core/Platform/PlatformCommon.hpp"

ark_global_var Keyboard::ChangeState_t g_callback_func = nullptr;

//
// Windows Callback Handler
//
//------------------------------------------------------------------------------
LRESULT CALLBACK
KeyboardHookProc(int const nCode, WPARAM const w_param, LPARAM const l_param)
{
    PKBDLLHOOKSTRUCT const keyboard_info = Cast<PKBDLLHOOKSTRUCT>(l_param);

    ark::KeyboardEvent const event = ark::Win32::TranslateWin32KeyToKeyboardEvent(
        Cast<u32>(w_param),
        keyboard_info->vkCode,
        keyboard_info->flags
    );
    g_callback_func(event);

    return CallNextHookEx(nullptr, nCode, w_param, l_param);
}


//------------------------------------------------------------------------------
HHOOK
Keyboard::RegisterHandler(ChangeState_t const callback_func)
{
    HHOOK const handler = SetWindowsHookEx(
        WH_KEYBOARD_LL,
        KeyboardHookProc,
        GetModuleHandle(nullptr),
        0
    );

    if(!handler) {
        Error::ExitWithMsg(ark::Platform::GetPlatformLastErrorAsString());
    }

    ARK_ASSERT_NOT_NULL(callback_func);
    g_callback_func = callback_func;

    return handler;
}

//------------------------------------------------------------------------------
void
Keyboard::UnregisterHandler(HHOOK const handle)
{
    if(!handle) {
        return;
    }

    if(!UnhookWindowsHookEx(handle)) {
        Error::ExitWithMsg(ark::Platform::GetPlatformLastErrorAsString());
    }

    g_callback_func = nullptr;
}
