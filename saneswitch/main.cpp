// std
// Arkadia
#include "ark_core/ark_core.hpp"
// saneswitcher
#include "Keyboard.hpp"
#include "Switcher.hpp"
#include "Tray.hpp"

//
// Types
//
//------------------------------------------------------------------------------
struct Globals
{
    // Windows Handles...
    HINSTANCE         app_instance         = nullptr;
    HHOOK             keyboard_hook        = nullptr;
    HWND              native_window_handle = nullptr;
    HICON             tray_icon            = nullptr;

    ark::Gfx::Window     *window           = nullptr;
    Switcher::SwitchData switch_data;
} G;


//
// Helper Functions
//
//------------------------------------------------------------------------------
ark_internal_function ark::String
GetIconFilename()
{
    ark::String path = ark::PathUtils::Join(
        ark::PathUtils::GetDefaultConfigDir(),
        "resources",
        "app_icon.ico"
    );

    return path;
}

//------------------------------------------------------------------------------
void
OnKeyStateChanged(ark::KeyboardEvent const &event)
{
    if(!event.is_alt_down && G.switch_data.has_data) {
        // ark::PrintLn("Destroying data");
        Switcher::DestroyData(&G.switch_data);
        return;
    }
    if(event.is_down && event.keycode == ark::KeyCodes::BackTick) {
        if(event.is_alt_down && !G.switch_data.has_data) {
            // ark::PrintLn("Creating data");
            Switcher::CreateData(&G.switch_data);
        }
        if(G.switch_data.has_data) {
            // ark::PrintLn("Switching data - {}", event.is_shift_down);
            Switcher::Switch(&G.switch_data, event.is_shift_down);
        }
    }
}


//
// Entry Point
//
//------------------------------------------------------------------------------
int
ark_main(ark::String const &command_line)
{
    ARK_UNUSED(command_line);

    G.app_instance         = GetModuleHandle(nullptr);
    G.window               = ark::Gfx::Window::Create();
    G.native_window_handle = Cast<HWND>(G.window->GetNativeHandle()),
    G.tray_icon            = Tray    ::LoadIcon       (GetIconFilename());
    G.keyboard_hook        = Keyboard::RegisterHandler(OnKeyStateChanged);

    Tray::DrawIcon(G.tray_icon, G.native_window_handle);

    ark::Gfx::Window::Event event;
    while(!G.window->WantsToClose()) {
        G.window->HandleEventsBlocking(&event, true);
        // @notice(stdmatt): Unfortunately the System Tray Icon has no
        // way to call us back but put a message on the WinProc Loop.
        // So to capture it we must handle the UnderlyingSystem event
        // and make sure that we are dealing with the same "id".
        if(event.type == ark::Gfx::Window::Event::Type::UnderlyingSystem) {
            ark::Gfx::Win32::WindowUnderlyingSystemEvent const &win32_event = event.underlying_system_data.data;
            if(win32_event.msg_type == Tray::WM_TRAY) {
                Tray::HandleTrayEvents(win32_event);
            }
        }
    }

    // @todo(stdmatt): Bulletproof this!!! What happen if we don't call it???  - Feb 11, 21
    Keyboard::UnregisterHandler(G.keyboard_hook);

    return 0;
}
