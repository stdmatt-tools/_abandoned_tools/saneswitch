#pragma once

// Arkadia
#include "ark_core/ark_core.hpp"

namespace Error {

template <typename ...Args>
void
ExitWithMsg(ark::String const &fmt, Args ...args)
{
    ark::PrintLn(fmt, args...);
    exit(1);
}

} // namespace Error
